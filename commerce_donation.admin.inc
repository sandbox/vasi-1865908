<?php

/**
 * @file
 *   Administration menu callbacks.
 */

/**
 * Menu callback for defining the donation amounts.
 */
function commerce_donation_amounts_page() {
  $form['commerce_donation_amounts'] = array(
    '#type' => 'textarea',
    '#title' => t('Predefined donation amounts'),
    '#default_value' => variable_get('commerce_donation_amounts', ''),
    '#description' => t('Enter a set of predifined donation amounts, one per line, in the form of <em>amount|description</em>. Example: "10|A very small amount.". This means 10 USD, if the USD is the currency of the site.')
  );
  return system_settings_form($form);
}