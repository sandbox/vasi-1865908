<?php

/**
 * @file
 *   Menu callbacks for the module.
 */

/**
 * Form constructor for the donation page.
 */
function commerce_donation_donate_page() {
  $prefined_amounts = commerce_donation_extract_predefined_amounts();
  $prefined_amounts[] = array('amount' => 'custom', 'description' => t('Custom amount'));
  $options = array();
  foreach ($prefined_amounts as $prefined_amount) {
    if ($prefined_amount['amount'] == 'custom') {
      $description = $prefined_amount['description'];
    }
    else {
      // The description should be somehow translatable...
      $description = t('!amount: @description', array('!amount' => commerce_currency_format($prefined_amount['amount'], commerce_default_currency(), NULL, FALSE),
                                                                            '@description' => $prefined_amount['description']));
    }
    $options[$prefined_amount['amount']] = $description;
  }
  $default_value = reset($prefined_amounts);
  $form['amount'] = array(
    '#type' => 'radios',
    '#title' => t('Predefined amounts'),
    '#options' => $options,
    '#default_value' => $default_value['amount'],
  );
  $form['custom_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom amount'),
    '#size' => 9,
    '#field_suffix' => commerce_default_currency(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Donate'),
  );
  return $form;
}

function commerce_donation_donate_page_validate($form, &$form_state) {
  // Check that, if the user selected a custom amount, the value is actually
  // a number.
  if ($form_state['values']['amount'] == 'custom') {
    if (!is_numeric($form_state['values']['custom_amount']) || $form_state['values']['custom_amount'] < 0) {
      form_set_error('custom_amount', t('Please enter a valid number for the amount field!'));
    }
  }
  else {
    // If the amount is not custom, just put the predefined value in the custom
    // amount field.
    form_set_value($form['custom_amount'], $form_state['values']['amount'], $form_state);
  }
}

function commerce_donation_donate_page_submit($form, &$form_state) {
  global $user;
  $order_id = commerce_cart_order_id($user->uid);
  if ($order_id) {
    $order = commerce_order_load($order_id);
  }
  else {
    $order = commerce_cart_order_new($user->uid);
  }
  $donation_product = commerce_product_load_by_sku('donation');
  // If we have a donation product, ad it to the cart.
  if ($donation_product) {
    // Make sure the order is empty.
    commerce_cart_order_empty($order);
    // Create the line item and put the custom donation amount in the variable
    // price field.
    $line_item = commerce_product_line_item_new($donation_product);
    $line_item->field_variable_price[LANGUAGE_NONE][0] = array('amount' => $form_state['values']['custom_amount'] * 100, 'currency_code' => $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code']);
    // Save the line item and add the product to the cart.
    commerce_line_item_save($line_item);
    commerce_cart_product_add($user->uid, $line_item);
  }
  $form_state['redirect'] = 'checkout';
}
